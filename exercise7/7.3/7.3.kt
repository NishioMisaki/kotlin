fun main(){
    while(true){
        println("振る回数n:")
        var n:Int?
        while(true){
            n = readLine()?.toInt()
            if ( n != null && n >=1 )
                break
            else println("自然数を入力して下さい。")
        }
        println("振る回数m(nより大きい回数):")
        var m:Int?
        while(true){
            m = readLine()?.toInt()
            if ( m != null && m >=1 )
                break
            else println("自然数を入力して下さい。")
        }
        val range = (1..10)
        var s_n:Int = 0 
        if ( n != null ){
            for ( i in 1 .. n ){
                if( range.random()==1 )
                s_n++
            }
            println("${n}回10面サイコロを振って1が出る確率:${s_n/n.toDouble()}")
        }
        var s_m:Int = 0
        if ( m != null ){
            for ( i in 1 .. m ){
                if( range.random()==1 )
                s_m++
            }
            println("${m}回10面サイコロを振って1が出る確率:${s_m/m.toDouble()}")
        }
        if( Math.abs(s_n/n!!.toDouble()-1.0/10)> Math.abs(s_m/m!!.toDouble()-1.0/10))
            break
        else
            println("もう一度やり直して下さい。")
    }
    println("大数法則が成立する。")
}