fun f(x:Double):Double{
    return Math.pow(x,3.0)-Math.atan(x)
}
fun main(){
    var x:Double // 面積内の選んだ点
    var y:Double // 面積内の選んだ点
    val n:Int = 10000000 // 選ぶ点の個数
    var i:Int = 0 // // 求める関数内に入っている点の個数
    var p:Double // 確率
    println("最大値=${f(10.0)}より大きい値Hを入力:")
    val h = readLine()?.toInt()
    if ( h != null ){
        for( j in 1 .. n ){
            x = (0..10).random().toDouble()
            y = (-h..h).random().toDouble()
            if( f(x) > 0 ){
                if( y<=0 && y>=f(x)) 
                    i++ 
            }
            else if( f(x) == 0.0 ){
                if( y == f(x) ) 
                    i++
            }
            else{
                if(y >= 0 && y <= f(x))
                    i++
            }
        }
        p = i/n.toDouble()
        println("積分値:${10*2*p*h}")
        println("(手計算の結果)積分値:${1.0/4*Math.pow(10.0,4.0)-10*Math.atan(10.0)+1.0/2*Math.log(1+Math.pow(10.0,2.0))}")
    }
}