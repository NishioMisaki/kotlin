fun main(){
    println("振る回数:")
    var n:Int?
    while(true){
        n = readLine()?.toInt()
        if ( n != null && n >=1 )
            break
        else println("自然数を入力して下さい。")
    }
    val range = (1..10)
    var s:Int = 0 
    if ( n != null ){
        for ( i in 1 .. n ){
            if( range.random()==1 )
            s++
        }
        println("${n}回10面サイコロを振って1が出る回数:${s}")
        println("${n}回10面サイコロを振って1が出る確率:${s/n.toDouble()}")
    }
}