fun f( x:Double ):Double{
    return -2*Math.pow(x,2.0)+10*x+3
}

fun main(){
    var x:Int // 面積内の選んだ点
    var y:Int // 面積内の選んだ点
    val n:Int = 1000000000
    var i:Int = 0 // 求める関数内に入っている点の個数
    val p:Double // 斜辺に含まれる確率
    println("最大値=${f(5.0/2)}より大きい値Hを入力:")
    val h = readLine()?.toInt()
    if( h != null ){
        for( j in i .. n ){
            val range1 = (0..5)
            val range2 = (0..h)
            x = range1.random()
            y = range2.random()
            if ( y <= f(x.toDouble()) )
                i++
        }
        p = i/n.toDouble() // 点が関数内にはいっている確率
        println("積分地:${5*p*h}")
        println("(手計算の結果)積分値:${-2.0/3*Math.pow(5.0,3.0)+5*Math.pow(5.0,2.0)+3*5}")
    }
}