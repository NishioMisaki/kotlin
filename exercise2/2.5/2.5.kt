fun function2(x:Double):Int{
    //ガウス記号を求める関数
    if ( x >= 0.0 ) return x.toInt()
    else return x.toInt()-1
}

fun main(){
    val x:String? = readLine()
    val y:Double? = x?.toDoubleOrNull()
    if( y != null ){
        println("[${y}]=${function2(y)}")
    }else{
        println("実装が入力されていません。")
    }
}