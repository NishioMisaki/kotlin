fun main(){
    println("2.4,2.5で作成した関数を利用して入力し値に対して1/2*x^2-5√(x^3)+2log[x]の値を求める。")
    val x:String? = readLine()
    val y:Double? = x?.toDoubleOrNull()
    if( y != null ) println("1/2*${y}^2-5√(${y}^3)+2log[${y}]=${function3(y)}")
    else println("実装が入力されていません。")
}
fun function3( x:Double ):Double{
    return 1.0/2.0*Math.pow(x,2.0)-Math.pow(x,3.0/5.0)+2*Math.log(function2(function1(x).toDouble()).toDouble())
}
fun function1( x:Double ):Int{
    //引数に対して、その絶対値を求める関数
    if( x >= 0 ) return x.toInt()/1
    else return -x.toInt()/1
}

fun function2(x:Double):Int{
    //ガウス記号を求める関数
    if ( x >= 0.0 ) return x.toInt()
    else return x.toInt()-1
}