fun function1( x:Double ):Double{ 
    //引数xに対して、x^3-2*x-8の値を変えす関数の作成
    return Math.pow(x,3.0)-2.0*x-8.0
}

fun main(){
    println("2.1で作成した関数を利用して入力された値xに対してx^3-2*x-8の値を計算表示する。")
    val x:String? = readLine()
    val y:Double? = x?.toDoubleOrNull()
    if( y != null ){
        println("${y}^3-2*${y}-8=${function1(y)}")
    }else{
        println("xに実装が入力されていません。")
    } 
}