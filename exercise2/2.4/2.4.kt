fun function1( x:Double ):Int{
    //引数に対して、その絶対値を求める関数
    if( x >= 0 ) return x.toInt()/1
    else return -x.toInt()/1
}

fun main(){
    val x:String? = readLine()
    val y:Double? = x?.toDoubleOrNull()
    if( y != null ){
        println("|${y}|=${function1(y)}")
    }else{
        println("実装が入力されていません。")
    }
}