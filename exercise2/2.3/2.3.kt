fun main(){
    println("入力された値xに対してx^3-2*x-8とx^2+3*x-2の大きい方の値を表示する。")
    val x:String? = readLine()
    val y:Double? = x?.toDoubleOrNull()
    if( y != null ){
        println("大きい値は、${bigger(function1(y),function2(y))}")
    }else{
        println("実装が入力されていません。")
    }
}

fun function1( x:Double ):Double {
    return Math.pow(x,3.0)-2*x-8
}

fun function2( x:Double ):Double {
    return Math.pow(x,2.0)+3*x-2
}

fun bigger( x:Double , y: Double ):Double {
    if( x > y ) return x
    else if ( x < y ) return y
    else return x
}
