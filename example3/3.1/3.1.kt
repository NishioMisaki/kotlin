fun plusf( x:Double, y:Double ):Double{
    return x+y
}

fun main(){
    println("x:")
    val x:String? = readLine()
    var a:Double? = x?.toDoubleOrNull()
    println("y:")
    val y:String? = readLine()
    val b:Double? = y?.toDoubleOrNull()
    if (  a != null && b != null ){
        a = plusf(a,b)
        println("x+y=${a}") 
    }else{
        println("実装が入力されていません。")
    }
}