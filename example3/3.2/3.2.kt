fun main(){
    println("a:")
    val a:String? = readLine()
    var x:Double? = a?.toDoubleOrNull()
    while(true){
        println("b:")
        val b:String? = readLine()
        val y:Double? = b?.toDoubleOrNull()
        if( x != null && y != null ){
            x = culc(x,y) 
        }
        if( x == 0.0 ) break
    }
}
fun culc( x:Double, y:Double ):Double{
    if( x == 0.0 || y == 0.0 || sum(x,y) == 0.0 ) println("処理を終了します。")
    else println(sum(x,y))
    return sum(x,y)
}
fun sum( x:Double, y:Double ):Double{
    return x+y
}