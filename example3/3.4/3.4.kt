fun main(){
    println("自然数を入力し、してください。x=")
    val x:String? = readLine()
    val a:Int? = x?.toIntOrNull()
    if( a != null ){
        println("${a}!=${kai(a)}")
    }
}
fun kai( x:Int ):Int{
    var n:Int 
    if( x == 1 ) n = 1
    else n = x*kai(x-1)
    return n 
}