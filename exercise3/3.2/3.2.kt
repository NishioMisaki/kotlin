fun geometricMean( x:Double,y:Double ):Double{
    return Math.sqrt(x*y)
}
fun main(){
    println("x:")
    val x:String? = readLine()
    val a:Double? = x?.toDoubleOrNull()
    println("y:")
    val y:String? = readLine()
    val b:Double? = y?.toDoubleOrNull()
    if( a != null && b != null ){
        println("相乗平均:${geometricMean(a,b)}")
    }
}