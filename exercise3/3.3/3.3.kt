fun arithmeticMean( x:Double ,y:Double ):Double{
    return (x+y)/2.0
}
fun geometricMean( x:Double,y:Double ):Double{
    return Math.sqrt(x*y)
}
fun max( x:Double,y:Double ):Double{
    if(x>y) return x
    else if(x<y) return y
    else return x
}
fun main(){
    println("x:")
    val x:String? = readLine()
    val a:Double? = x?.toDoubleOrNull()
    println("y:")
    val y:String? = readLine()
    val b:Double? = y?.toDoubleOrNull()
    if( a != null && b != null ){
        println("${a}と${b}の相加平均と相乗平均の大きさ方は、${max(arithmeticMean(a,b),geometricMean(a,b))}")
    }
}