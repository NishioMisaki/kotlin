fun arithmeticMean( x:Double ,y:Double ):Double{
    return (x+y)/2.0
}
fun main(){
    println("x:")
    val x:String? = readLine()
    val a:Double? = x?.toDoubleOrNull()
    println("y:")
    val y:String? = readLine()
    val b:Double? = y?.toDoubleOrNull()
    if( a != null && b != null ){
        println("相加平均:${arithmeticMean(a,b)}")
    }
}