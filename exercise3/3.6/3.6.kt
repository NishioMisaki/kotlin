fun fiveArithmeticMean(a:Double,b:Double,c:Double,d:Double,e:Double):Double{
    return (a+b+c+d+e)/5.0
}
fun decentralization( a:Double , b:Double , c:Double , d:Double , e:Double ):Double{
    var sum:Double = 0.0
    for ( i in 1 .. 5 ){
        sum = sum +( fiveArithmeticMean(a,b,c,d,e) ) 
    }
    return sum/5.0
}
fun standardDeviation(x:Double):Double{
    return Math.sqrt(x)
}
fun standardValue(n:Int , a:Double , b:Double , c:Double , d:Double , e:Double ):Double{
    var m:Double = 0.0
    if( n == 0 ) m = a
    if( n == 1 ) m = b
    if( n == 2 ) m = c
    if( n == 3 ) m = d
    if( n == 4 ) m = e
    return (10*(m-fiveArithmeticMean(a,b,c,d,e)))/standardDeviation(decentralization(a,b,c,d,e))+50.0
}
fun main(){
    val number = readLine()!!.split(" ").map{it.toDouble()}
        println("相加平均:${fiveArithmeticMean(number[0],number[1],number[2],number[3],number[4])}")
        println("分散:${decentralization(number[0],number[1],number[2],number[3],number[4])}")
        println("標準偏差:${standardDeviation(decentralization(number[0],number[1],number[2],number[3],number[4]))}")
        println("偏差値:${standardValue(5,number[0],number[1],number[2],number[3],number[4])}")
    }