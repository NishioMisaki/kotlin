fun fiveArithmeticMean(a:Double,b:Double,c:Double,d:Double,e:Double):Double{
    return (a+b+c+d+e)/5.0
}
fun decentralization( a:Double , b:Double , c:Double , d:Double , e:Double ):Double{
    var sum:Double = 0.0
    for ( i in 1 .. 5 ){
        sum = sum +( fiveArithmeticMean(a,b,c,d,e) ) 
    }
    return sum/5.0
}
fun standardDeviation(x:Double):Double{
    return Math.sqrt(x)
}
fun main(){
    println("x:")
    val x:String? = readLine()
    val a:Double? = x?.toDoubleOrNull()
    println("y:")
    val y:String? = readLine()
    val b:Double? = y?.toDoubleOrNull()
    println("y:")
    val z:String? = readLine()
    val c:Double? = z?.toDoubleOrNull()
    println("y:")
    val o:String? = readLine()
    val d:Double? = o?.toDoubleOrNull()
    println("y:")
    val p:String? = readLine()
    val e:Double? = p?.toDoubleOrNull()
    if( a != null && b != null && c != null && d != null && e != null ){
        println("相加平均:${fiveArithmeticMean(a,b,c,d,e)}")
        println("分散:S{decentralization(a,b,c,d,e)}")
        println("標準偏差:${standardDeviation(decentralization(a,b,c,d,e))}")
    }
}