fun main(){
    println("x:")
    val x = readLine()?.toDouble()?:0.0
    println(if(x>=0) 1 else 0)
}