fun main(){
    var max:Double 
    val number = readLine()!!.split(" ").map{it.toDouble()}
    max = number[0]
    for ( i in 0 .. number.size -1  ){
        if( max < number[i] ) max = number.get(i)
    }
    println("最大値:${max}")
}