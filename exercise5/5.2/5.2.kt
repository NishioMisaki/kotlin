import java.util.*

fun main(){
    val x :Array<Int> = arrayOf(2,1,4,5,7,8,9,4,3,2,1,4,0,3,12,13,10,2,4,12,5,7,8,6,4,3,6,5,4,2,5,3,2,5,6,7,4,2,1,3)
    var m:Int
    for (i in x.size -2 downTo 0 ){
        for ( j in 0 .. i ){
                if(x[j+1]<x[j]){
                    m = x[j+1]
                    x[j+1] = x[j]
                    x[j] = m
                }
        }
    }
    println(Arrays.toString(x))
}
