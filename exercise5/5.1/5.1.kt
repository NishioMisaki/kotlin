import java.util.*

fun main(){
    val x :Array<Int> = arrayOf(2,1,4,5,7,8,9,4,3,2,1,4,0,3,12,13,10,2,4,12)
    var m:Int
    var n:Int
    for ( i in 0 .. x.size-2 ){
        n = x[i]
        m = i 
        for ( j in i .. x.size -1 ){
            if( n > x[j] ){
                n = x[j]
                m = j
            }
        }
        x[m] = x[i]
        x[i] = n
    }
    println(Arrays.toString(x))
}