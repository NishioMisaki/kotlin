fun main(){
    println("キーボードから実数xを入力し、x^3-1/2*x^2-1の結果を出力する。")
    val x:String? = readLine()
    val y:Double? = x?.toDoubleOrNull()
    if( y != null ){
        println("x^3-1/2*x^2-1=${Math.pow(y,3.0)-1.0/2.0*Math.pow(y,2.0)-1}")
    }else{
        println("実数が入力されていません。")
    }

}