fun main(){
    println("キーボードから実数xを入力し、tanθ=xとなる度数θを求める。")
    val x:String? = readLine()
    val y:Double? = x?.toDoubleOrNull()
    if( y != null ){
        println("arctan${y}=${Math.atan(y)*180/Math.PI}")
    }else{
        println("実装が入力されていません。")
    }
}