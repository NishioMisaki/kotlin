fun main(){
    println("キーボードから実数xを入力し、xラジアンにおける正弦の値を出力する。")
    val x:String? = readLine()
    val y:Double? = x?.toDoubleOrNull()
    if( y != null ){
        println("sin${y}=${Math.sin(y)}")
    }else{
        println("実装が入力されていません。")
    }
}