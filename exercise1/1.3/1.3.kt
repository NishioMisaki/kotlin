fun main(){
    println("キーボードから実数xを入力し、0<=x<=360ならばx度における余弦の値を出力する。")
    val x:String? = readLine()
    val y:Double? =x?.toDoubleOrNull()
    if( y != null ){
        if( 0 <= y && y <= 360 ){
            println("cos${y}=${Math.cos(y*Math.PI/180.0)}")
        } 
    }else{
        println("実装が入力されていません。")
    }

}