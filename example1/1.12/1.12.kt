fun main(){
    // 無限ループとbreak文の使用例
    var i:Int = 2
    println("初項2,公差7の等差数列の項で50以下の項は順に、")
    while (true) {
        println(i)
        i=i+7
        if(i>50) break
    }
    println("となっている。")
}