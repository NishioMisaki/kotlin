fun main(){
    // インクリメント・デクリメントの演算のオーバーロード
    data class Counter(val num: Int) {
        operator fun inc(): Counter {
          return Counter(num + 1)
        }
      operator fun dec(): Counter {
          return Counter(num -1)
      }
  }

  var a = Counter(1) 
  var b = Counter(1)
  var c = Counter(1)
  var d = Counter(1)
  println(a++)
  println(++b)
  println(c--)
  println(--d)

  /**
   * 前置演算子
   * a=1,b=++aとする
   * 計算手順
   * ➀a=a+1
   * ➁b=a
   * (a,b)=(2,2)
   * 
   * 後置演算子
   * a=1,b=a++とする
   * 計算手順
   * ➀b=a
   * ➁a=a+1
   * (a,b)=(2,1)
   */
}