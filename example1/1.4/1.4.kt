fun main(){
    // コレクションの利用(Set)
    // Set
    val number01: Set<Int> = setOf<Int>(3,2,1)
    val number02 = setOf(1,2,3,4,3,2,1)
    val number03 = setOf(2,1,3)
    println("Setの読み出し")
    println("${number01}/${number02}/${number03}")
    println("number01とnumber03の整合性:${number01==number03}")

    // イミュータブルなSet
    val number11: MutableSet<Int> = mutableSetOf(1,2,3)
    val number12: MutableSet<Int> = mutableSetOf(4,3,2,1)
    number11.add(4)
    println("イミュータブルなnumber11:${number11}")
    number11.add(2)
    println("イミュータブルなnumber11:${number11}")
    println("number11とnumber12の整合性:${number11==number12}")

    // SortedSet
    val number31 = sortedSetOf(3,2,1,5)
    println("ソートSetnumber31:${number31}")
    number31.add(4)
    println("追加後のソートSetnumber31:${number31}")
}