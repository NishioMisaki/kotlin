fun main(){
    // 算術演算子の利用
    val x:Int = 5
    val y:Int = 2
    val a:Double = 5.0
    val b:Double = 2.0
    println("a+b=${a+b}")
    println("a-b=${a-b}")
    println("a*b=${a*b}")
    println("a/b=${a/b}")
    println("x/y=${x/y}")
    println("x%y=${x%y}")
}