fun main(){
    // 算術演算子のオーバーロード

    class Point1 (var x:Double,var y:Double ) {
        operator fun plus( other:Point1 ):Point1{
            return Point1(this.x+other.x,this.y+other.y)
        }
        operator fun minus( other:Point1 ):Point1{
            return Point1(this.x-other.x,this.y-other.y)
        }
        operator fun times( other:Double ):Point1{
            return Point1(this.x*other,this.y*other)
        }
        operator fun unaryMinus():Point1{
            return Point1(-this.x,-this.y)
        }
    }
    
    val a = Point1(3.0,4.0)
    val b = Point1(2.0,4.0)
    val c = a + b 
    val d = a - b
    val e = a*10.0
    val f = -a
    println("a:(${a.x},${a.y})")
    println("b:(${b.x},${b.y})")
    println("c:(${c.x},${c.y})")
    println("d:(${d.x},${d.y})")
    println("e:(${e.x},${e.y})")
    println("f:(${f.x},${f.y})")

}