// 数学関数の利用
import kotlin.math.*

fun main(){
    val x:Double = -4.0
    val y:Double = 3.0
    println("x^y=${Math.pow(x,y)}")
    println("√y=${Math.sqrt(y)}")
    println("log y =${Math.log(y)}")
    println("log_10=${Math.log10(y)}")
    println("e^x=${Math.exp(x)}")
    println("|x|=${Math.abs(x)}")
    println("cos π=${Math.cos(3.14159265358979)}")
    println("arcsin(1/2)=${Math.asin(0.5)}")
    println("度数表示:arcsin(1/2)=${Math.asin(0.5)/3.14159265358979*180}")
}