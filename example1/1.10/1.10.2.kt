fun main(){
    // for文の使用例
    println("初項2,公差7の等差数列の項で50以下の項は順に、")
    for ( i in 2 .. 50 step 7 ){
        println(i)
    }    
    println("となっている。")
}