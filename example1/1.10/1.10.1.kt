fun main (){
    // for文の利用(増減)
    val x:Int = 7
    println("for文:増減")
    for ( i in 0..10 step 1 ){
        println("i=${i}")
        println("i+$x=${i+x}")
    }
    // for文の利用(加減)
    println("for文:加減")
    for ( i in 10 downTo 0 step 1 ){
        println("i=${i}")
        println("i+$x=${i+x}")
    }
}