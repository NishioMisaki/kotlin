fun main(){
    // コレクションの利用(list)
    // 読み取り専用
    val number1: List<Int> = listOf(10,20,30,40)
    //val number1dash: List<Double> = listOf<Double>(1.0,2.0,3.0,4.0)
    //val characters1: List<String> = listOf("a","b","c")

    // ミュータブルなリスト
    val number2: MutableList<Int> = mutableListOf(10,20,30,40)
    val number2dash: MutableList<Double> = mutableListOf(1.0,2.0,3.0,4.0,1.0)
    val characters2: MutableList<String> = mutableListOf("a","b","c")

    // リストの追加
    number2.add(50)
    // リストの削除
    number2dash.remove(1.0)
    //　リストの変更
    characters2.set(1,"y")

    // 個々のリストを読みだす
    println("個々のリストを読み出し")
    println("x[0]=${number1.get(0)}")
    println("x[1]=${number1.get(1)}")
    println("x[2]=${number1.get(2)}")

    // リストの読み出し
    println("ミュータブルなリストの読み出し")
    println("可変整数値２:$number2")
    println("可変実数値２:$number2dash")
    println("可変文字列２:$characters2")

    // ミュータブルなリストに読み取り専用変数を代入する
    //　ミュータブルなリストの変更が読み取り専用に影響する
    val number3: MutableList<Int> = mutableListOf(1,2,3)
    val number3dash: List<Int> = number3
    number3.add(4)
    println("可変変更に読み取り専用を代入する:$number3dash")
}