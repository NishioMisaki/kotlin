fun main(){
    // while文の使用例
    var i:Int = 2
    println("初項2,公差7の等差数列の項で50以下の項は順に、")
    while(i<=50){
        println(i)
        i=i+7
    }
    println("となっている。")
}