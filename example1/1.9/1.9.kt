fun main(){
    // if文の使用
    println("実数を入力して下さい。")
    val x:String? = readLine()
    val y:Double? = x?.toDoubleOrNull() 
    /**
     * toDoubleOrNull(String) 
     * 引き数：Sting型
     * 戻り値：Double?型　※引き数がnullの場合は0を返す
     * */
    if ( y != null ){
        if( y == 0.0 ) println("入力されたのは0です。")
        else if( y < 0.0 ){
            println("入力されたのは負の数です。")
            println("入力された値の絶対値は${-y}です。")
        }
        else println("入力されたのは正の数です。")
    }
    if( y == null ) println("実数が入力されていません。")
}
    