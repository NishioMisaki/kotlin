fun main(){
    // コレクションの利用(Map)
    // Map
    val alphabet01: Map<String,Int>
    alphabet01 = mapOf<String,Int>("Kotlin" to 6,"java" to 4 , "Python" to 6)
    println("Mapのalphabet01:${alphabet01}")

    val alphabet02: Map<String,Int>
    alphabet02 = mapOf<String,Int>("Python" to 6,"Kotlin" to 6,"java" to 4)
    println("Mapのalphabet02:${alphabet02}")

    val alphabet03: Map<String,Int>
    alphabet03 = mapOf<String,Int>("Kotlin" to 6,"java" to 4,"Kotlin" to 6)
    println("同じキー値がある場合:${alphabet03}")
    // キー値と値が等しいならtrue　※順番は関係ない
    println("alphabet01とalphabet02の統合性:${alphabet01==alphabet02}")

    // イミュータブルなMap
    val alphabet11 = mutableMapOf("Kotlin" to 6,"java" to 4)
    val alphabet12 = alphabet11.plus("Python" to 6)
    println("イミュータブルなalphabet11:${alphabet11}")
    println("イミュータブルなalphabet12:${alphabet12}")

    // SortedMap
    val alphabet21 = sortedMapOf("Python" to 6,"Kotlin" to 6, "java" to 4)
    // 文字列はアルファベット順にソートされる
    println("並び替えalphabet21_keys:${alphabet21.keys}")
    // 数値は昇順に並び替えられる
    println("並び替えalphabet21_valuses:${alphabet21.values}")
    // 全体は、valusesとkeysを総合して並び替える
    println("並び替えalphabet21:${alphabet21}")
}