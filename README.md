# kotlin

Kotlin練習用branch

- 目次
    - [コード構成](#コード構成)
    - [環境構築](#環境u構築)
    - [実行方法](#実行方法)
    - [数学関数](#数学関数)
    - [モンテカルロ法](#モンテカルロ法)

#### コード構成

大学時代のサンプル問題を利用して、kotlinでソースコードを実装。
exampleは教授が作成した例題であり、exerciseは例題を元に、課題として出題された問題である。

#### 環境構築

 - [kotlin](https://github.com/JetBrains/kotlin/releases/tag/v1.3.72)のインストール
 下記画像の赤線部のリンクをインストール
 ![](スクリーンショット\スクリーンショットkotlinインストール画面.png)
 - インストールし解凍、環境構築を設定する
 - 「kotlin -versioin」でバージョンが記述されればインストール完了

#### 実行方法

 - サンプルコードの作成
  sample.kt
  
  ```kotlin
  fun main(args: Array<String>){
      println("Hello, world!")}
  ```
 
 - コンパイル
  ```
   $ kotlin sample.kt -includ-runtime -d sample.jar
  ```
 - 実行
  ```
  $ java -jar sample.jar
  Hello, world!
  ```
  `kotlinc` でコンパイルしたアプリケーションの実行には、Kotlin ランタイムが必要です。
   `kotlinc` の <span style="color: red; ">-include-runtime</span> オプションは、生成する JAR ファイル内にこの Kotlin ランタイムを含めてしまう指定です。 このようにして作成した JAR ファイルは、上記のように単独で実行することができます。

#### 数学関数

 [kotlin.Mathパッケージ](https://runebook.dev/ja/docs/kotlin/api/latest/jvm/stdlib/kotlin.math/index)

 #### モンテカルロ法
kotlinの乱数を使用した モンテカルロ法は手計算と大きな差異がある。
その為、数値積分の基本を使用する。
 - 長方形近似
 　$\int_{a}^{b} f(x) = \sum_{i=0}^{n-1} f(x_i)(x_{i+1}-x_i) $
 - 台計則
  $\int_{a}^{b} f(x) = \frac{1}{2}\sum_{i=0}^{n-1}f(x_{i+1}+f(x_i))(x_{i+1}-x_i)$