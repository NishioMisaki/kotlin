fun main(){
    println("x:")
    val x = readLine()?.toDouble()
    println("y:")
    val y = readLine()?.toDouble()
    if( x != null && y != null ){
        val z = sum(x,y)
        println("z:${z}")
    }
}
fun sum( x:Double, y:Double ):Double{
    return x+y
}