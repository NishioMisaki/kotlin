// 関数の作成(関数を先に記述)
// 関数を記述する順序は関係ない
fun bai( x:Double ):Double{
    return x*2
}

fun main(){
    println("y:")
    val y:String? = readLine()
    val z:Double? = y?.toDoubleOrNull()
    if( z != null ){
        println("${z}*2=${bai(z)}")
    }else{
        println("実装が入力されていません。")
    }
}
