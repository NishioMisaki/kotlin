// 関数における変数の有効範囲
fun plusf( x:Double ):Double {
    return x + 3.0 
} 

fun main(){
    var x:Double
    println("y:")
    val y:String? = readLine()
    var z:Double? = y?.toDoubleOrNull()

    if( z != null ){
        x = 2*z
        z = plusf(z)
        x = x + z
        println("x:${x}") 
    }else{
        println("yに実装が入力されていません。")
    }
}