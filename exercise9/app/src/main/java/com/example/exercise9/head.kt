package com.example.exercise9

import kotlin.math.*

fun f( z:Double ):Double{
    return exp(4* sin(atan(PI *z)))
}

fun Rectangle( n:Int, x:Double, y:Double ):String{
    var Rectangle:Double = 0.0 // 長方形近似
    var s:Array<Double> = arrayOf(0.0,0.0)
    for ( i in 1 .. n-1 ){
        s[0] = s[1]
        s[1] = abs(x-y) /n*i
        Rectangle = Rectangle + f(s[0])*(s[1]-s[0])
    }
    return Rectangle.toString()
}

fun Trapezoid( n:Int, x:Double, y:Double ):String{
    var Trapezoid:Double = 0.0 // 長方形近似
    var s:Array<Double> = arrayOf(0.0,0.0)
    for ( i in 1 .. n-1 ){
        s[0] = s[1]
        s[1] = abs(x-y) /n*i
        Trapezoid = Trapezoid + 1.0/2*(f(s[1])+f(s[0]))*(s[1]-s[0])
    }
    return Trapezoid.toString()
}