package com.example.exercise9

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.example.exercise9.databinding.ActivityMainBinding
import ru.noties.jlatexmath.JLatexMathDrawable

class MainActivity : AppCompatActivity() {
    @SuppressLint("WrongViewCast")

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //val latex = "\\int_{x}^{y}e^{4\\sin(\\atan^(πz))dz"
        val latex2 = "∫e^(sin(atan(πz))dzにおけるx~yの積分値を計算"
        //val drawable = JLatexMathDrawable.builder(latex)
        //    .textSize(70f)
        //    .padding(8)
        //    .background(-0x1)
        //    .align(JLatexMathDrawable.ALIGN_RIGHT)
        //   .build()
        val n = findViewById<EditText>(R.id.n);
        val x = findViewById<EditText>(R.id.x);
        val y = findViewById<EditText>(R.id.y);
        binding.title.text = latex2

        binding.button.setOnClickListener {
            val n_s:Int = n.getText().toString().toInt();
            val x_s:Double = x.getText().toString().toDouble();
            val y_s:Double = y.getText().toString().toDouble();
            binding.rectangleAnwser.setText(Rectangle(n_s, x_s, y_s));
            binding.trapezoidAnwser.setText(Trapezoid(n_s,x_s,y_s));
        }
    }


    @SuppressLint("WrongViewCast")
    override fun onResume() {
        super.onResume()
    }
}
