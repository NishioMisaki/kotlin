import kotlin.random.Random

fun main(){
    // 固定されたseed値でRamdom関数の使用
    fun getRandomList(random:Random):List<Int> = List(10){ random.nextInt()}
    /**
     * fun Random(seed:Long):Rondom
     * 指定されたシード Long 値でシードされた繰り返し可能な乱数ジェネレーターを返します。
     * 
     * nextInt()
     * 乱数ジェネレーターから「Int型」の乱数を取得します。※Int.MIN_VALUE~Int.MAX_VALUE
     * 
     * nextInt(until:Int)
     * 指定された乱数ジェネレーターからバインドされるまでの「Int型」の負でないランダムを取得する。
     * 
     * */ 
    val randomValues1 = getRandomList(Random(42))
    println(randomValues1)
    val randomValues2 = getRandomList(Random(42))
    println("randomValues1 == randomValues2 is ${randomValues1 == randomValues2}") 
    val randomValues3 = getRandomList(Random(0))
    println(randomValues3)
}