fun main(){
    /**
     * Range.random()
     * 任意の数の範囲が定義されたRangeオブジェクトの random()メソッドを呼び出すと、
     * その範囲の乱数が返されます。
     */
    val range = (1..6)
    for (i in 1 .. 5 ){
        println("${i}回目の試行結果:${range.random()}")        
    }
}