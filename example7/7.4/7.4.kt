import kotlin.random.Random

fun main(){
    // 固定されたseed値でRamdom関数の使用
    // 何度試行しても結果は変わらない
    fun getRandomList(random:Random):List<Int> = List(1){ random.nextInt(0,Int.MAX_VALUE)}
    for ( i in 1 .. 5 ){
        val randomValues1 = getRandomList(Random(i))
        println("${i}回目の試行結果:${(randomValues1[0].toDouble()/(Int.MAX_VALUE+1.0)*6).toInt()+1}")
    }
}