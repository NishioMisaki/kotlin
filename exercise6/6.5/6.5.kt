fun f(x:Double):Double{
    return Math.pow(x,3.0)-Math.atan(x)
}

fun main(){
    println("実数yを入力し、f(x)=x^3-atan(x)に対するf(y)'を出力")
    println("h=10^")
    val x:String? = readLine()
    val index:Double? = x?.toDoubleOrNull()
    var h:Double
    println("y=")
    val y:String? = readLine()
    val a:Double? = y?.toDoubleOrNull()
    if ( index != null && a != null ){
        h = Math.pow(10.0,index)
        println("f(${a})'=${(f(a+h)-f(a))/h}")
        println("(注意)手計算の結果:f(${a})'=${3*Math.pow(a,2.0)-1.0/(1+Math.pow(a,2.0))}")
    }
}