fun f(x:Double):Double{
    return Math.pow(x,3.0)-Math.atan(x)
}
fun main(){
    println("実数yを入力し、f(x)=x^3-atan(x)に対するf(y)'を出力")
    println("y=")
    var s:Array<Double> = arrayOf(0.0,0.0)
    var Rectangle:Double = 0.0 // 長方形近似
    var Trapezoid:Double = 0.0 // 台形近似
    var a:Double?
    while(true){
        val y:String? = readLine()
        a = y?.toDoubleOrNull()
        if ( a != null && a < 0 )
            println("入力し直して下さい。")
        else
            break
    }
    println("n(0~yを等分する値)を入力:")
    val x:String? = readLine()
    val n:Int? = x?.toIntOrNull()
    if ( n != null && a != null ){
        for( i in 1 .. n-1 ){
            s[0]=s[1]
            s[1]=a/n*i
            Rectangle = Rectangle + f(s[0])*(s[1]-s[0])
            Trapezoid = Trapezoid + 1.0/2*(f(s[1])+f(s[0]))*(s[1]-s[0])
        }
    println("∫f(x)(0~${a})=${Rectangle} (注意)長方形近似")
    println("∫f(x)(0~${a})=${Trapezoid} (注意)台形近似")
    println("(注意)手計算の結果:∫f(x)(0~${a})=${1.0/4.0*Math.pow(a,4.0)-a*Math.atan(a)+1.0/2.0*Math.log(1+Math.pow(a,2.0))}")
    }
}