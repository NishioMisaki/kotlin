fun kai( x:Int ):Int{
    var kai:Int = 1
    if( x == 0 )
        kai = 1
    else{
        for( i in 1 .. x ){
            kai = kai * i
        }
    }
    return kai
}

fun main(){
    println("eの値をマクローリン展開を利用して求める。")    
    println("ε(絶対誤差)=10^")
    val x:String? = readLine()
    val index:Double? = x?.toDoubleOrNull()
    var ε:Double
    var s:Double = 0.0
    var n:Int = 0
    val y:Double = 1.0
    if ( index != null ){
        ε = Math.pow(10.0,index)
        while(n<13){
            s = s + Math.pow(y,n.toDouble())/kai(n)
            if( Math.abs(Math.exp(1.0)-s)<ε)
                break
            n++
        }
    }
    println("極限値は、$s")
    println("(注意)e=${Math.exp(1.0)}")
}