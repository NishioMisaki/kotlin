fun main(){
    println("a(n)=Σ(i^2)/n^3の極限値を出力")
    println("ε=10^")
    val x:String? = readLine()
    val index:Double? = x?.toDoubleOrNull()
    var n:Double = 1.0
    var s:Array<Double> = arrayOf(0.0,0.0)
    var v:Array<Double> = arrayOf(0.0,0.0)
    var ε:Double
    if( index != null )
    {
        ε = Math.pow(10.0,index) 
        while( n > 0 ){
            v[0]=v[1] //Σ(i^2) i=1~n+1
            s[0]=s[1] //a(n)
            v[1]=v[0]+Math.pow(n,2.0)
            s[1]=v[1]/Math.pow(n,3.0) 
            if( Math.abs(s[0]-s[1])<ε)
                break
            n++
        }
    }
    println("極限値は、${s[1]}")
    println("手計算の結果は、${1.0/3.0}")
}