fun main(){
    println("x:")
    val x:String? = readLine()
    val y:Double? = x?.toDoubleOrNull()
    var a:Double
    if ( y != null ){
        a = y*(y*(y*(3.0/8.0*y+1.0/4.0)-2.0/5.0)-1.0/2.0)-1.0/2.0
        println("3/8x^4+1/4x^3-2/5x^2-1/2x-1/2=${a}")
    }
}