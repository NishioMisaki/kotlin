fun kai( x:Int ):Int{
    var kai:Int = 1
    if( x == 0 )
        kai = 1
    else{
        for( i in 1 .. x ){
            kai = kai * i
        }
    }
    return kai
}

fun main(){
    println("cos(0.3)の値をマクローリン展開を利用して求める。")    
    println("ε(絶対誤差)=10^")
    val x:String? = readLine()
    val index:Double? = x?.toDoubleOrNull()
    var ε:Double
    var s:Double = 0.0
    var n:Int = 0
    val y:Double = 0.3
    if ( index != null ){
        ε = Math.pow(10.0,index)
        while(2*n<13){
            s = s + +Math.pow(-1.0,n.toDouble())*Math.pow(y,2.0*n)/kai(2*n)
            if(Math.abs(Math.cos(0.3)-s)<ε)
                break
            n++
        }
    } 
    println("極限値は、$s")
    println("(注意)cos(0.3)=${Math.cos(0.3)}")
}